
const EchartCompMixin = {
  props: {
    location: {
      type: String,
      default: ''
    }
  },
  data() {
    return {
      chart: null,          // 存储图表实例
      echartThemeName: '',  // echart主题名称
      sourceData: null,     // 存储接口返回的源数据
    }
  },
  created () {

  },
  mounted () {
    if(this.pageMetadata.ownEchartTheme == '1' && this.pageMetadata.echartThemeId) {
      this.echartThemeName = 'globalEchartTheme';
      this.$Echarts.registerTheme(this.echartThemeName, JSON.parse(this.pageMetadata.echartThemeJsonText));
    }else {
      if(this.projectInfo.echartThemeId && this.projectInfo.echartThemeJsonText) {
        this.echartThemeName = 'globalEchartTheme';
        this.$Echarts.registerTheme(this.echartThemeName, JSON.parse(this.projectInfo.echartThemeJsonText));
      }
    }

    this.chart = this.$Echarts.init(document.getElementById('chart-'+this.component.id), this.echartThemeName);
    this.drawChart();
  },
  beforeDestroy () {
    this.chart.dispose()
  },
  methods: {
    resizeHandle () {
      this.chart.resize();
    },

    /**
     * 绘制图表，初始化接口返回的源数据
     */
    drawChart () {
      let _this = this;

      this.chart.setOption(this.component.compConfigData.chartOption);



    },

    /**
     * 刷新图表
     */
    refreshChart () {
      let _this = this;
      this.chart.setOption(this.component.compConfigData.chartOption);

    }
  },
  computed: {
    projectInfo () {
      if(this.$store.state.release.pageMetadata) {
        return this.$store.getters['release/getProjectInfo']
      }else {
        return this.$store.getters['designer/getProjectInfo']
      }
    },
    pageMetadata () {
      if(this.$store.state.release.pageMetadata) {
        return this.$store.getters['release/getPageMetadata']
      }else {
        return this.$store.getters['designer/getPageMetadata']
      }
    },
    component () {
      if(this.$store.state.release.pageMetadata) {
        return this.$store.getters['release/getLayoutItemById'](this.location).component
      }else {
        return this.$store.getters['designer/getLayoutItemById'](this.location).component
      }
    }
  },
  watch: {
    'component.compConfigData.chartOption': {
      handler: 'drawChart',
      deep: true
    }
  }
};

export default EchartCompMixin
